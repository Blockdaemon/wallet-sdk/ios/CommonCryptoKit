// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "CommonCryptoKit",
    platforms: [
        .iOS(.v16)
    ],
    products: [
        .library(
            name: "CommonCryptoKit",
            targets: ["CommonCryptoKit"]),
    ],
    dependencies: [
        .package(
            url: "https://github.com/TakeScoop/SwiftyRSA.git",
            exact: "1.8.0"
        )
    ],
    targets: [
        .target(
            name: "CommonCryptoKit",
            dependencies: [
                .product(name: "SwiftyRSA", package: "SwiftyRSA")
            ]
        ),
        .testTarget(
            name: "CommonCryptoKitTests",
            dependencies: ["CommonCryptoKit"]),
    ]
)
