// Copyright © Blockdaemon All rights reserved.

import CryptoKit
import Foundation

extension NSData {
    public var hexValue: String {
        (self as Data).hexValue
    }
}

extension Data {
    public var hexValue: String {
        map { String(format: "%02x", $0) }.reduce(into: "") { $0.append($1) }
    }

    public var bytes: [UInt8] {
        Array(self)
    }
}

public protocol DataRepresentable: Hashable {
    var data: Data { get }

    init(data: Data) throws
}

public protocol HexRepresentable: DataRepresentable, LosslessStringConvertible, CustomDebugStringConvertible {
    var hexValue: String { get }
}

extension HexRepresentable {
    public var description: String {
        data.hexValue
    }
}

extension HexRepresentable {
    public var debugDescription: String {
        data.hexValue
    }
}

extension HexRepresentable {
    public var hexValue: String {
        description
    }
}

extension CryptoKit.Digest {
    public var bytes: [UInt8] { Array(makeIterator()) }
    public var data: Data { Data(bytes) }
}
