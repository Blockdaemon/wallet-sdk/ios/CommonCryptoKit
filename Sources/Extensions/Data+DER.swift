// Copyright © Blockdaemon All rights reserved.

import Foundation
import SwiftyRSA

extension Data {
    public func encodePublicKeyToDER() -> Data? {
        let publicKey = try? PublicKey(data: self).data()
        return publicKey!.prependx509Header()
    }
    
    public func encodePrivateKeyToDER() -> Data {
        prependx509Header()
    }
}
