// Copyright © Blockdaemon All rights reserved.

import Foundation

public struct SigningInput: Decodable {
    public let index: Int
    public let value: String
    
    public init(index: Int, value: String) {
        self.index = index
        self.value = value
    }
}
