// Copyright © Blockdaemon All rights reserved.

import Foundation

public struct SignedInputs: Encodable {
    public let index: Int
    public let value: String
    public let recoveryId: Int?
    
    public init(index: Int, value: String, recoveryId: Int? = nil) {
        self.index = index
        self.value = value
        self.recoveryId = recoveryId
    }
}
