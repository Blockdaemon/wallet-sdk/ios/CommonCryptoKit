// Copyright © Blockdaemon All rights reserved.

import Foundation

public struct ChainMetadata: Decodable {
    public let blockchain: Blockchain
    public let chainPath: [Int]
    public let curveName: Curve
    public let chainId: Int
    public let keyType: String
    public let id: String
}
