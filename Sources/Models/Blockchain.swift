// Copyright © Blockdaemon All rights reserved.

public enum Blockchain: String, Codable {
    case bitcoin
    case ethereum
    case solana
    case polkadot
}
