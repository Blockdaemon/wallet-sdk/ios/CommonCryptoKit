// Copyright © Blockdaemon All rights reserved.

public enum Curve: String, CaseIterable, Codable, Equatable {
    // ECDSA
    case secp256k1
    // EDDSA
    case ed25519 = "ED-25519"
}
